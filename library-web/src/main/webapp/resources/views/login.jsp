<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="e" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><fmt:message key="login.title"/></title>
</head>
<body onload='document.f.username.focus();'>
    <h3 align="center">
        <fmt:message key="login.welcomeMessage"/>
    </h3>

    <div>
        <fmt:message key="login.inviteMessage"/>
    </div>

    <e:error error="${error}" messageKey="login.error.failMessage"/>

    <form id="login-form" name="f" method="POST" action=<%=response.encodeURL("/login")%>>
        <table id="login-data">
            <tr>
                <td>
                    <fmt:message key="login.user"/>
                </td>
                <td>
                    <input type="text" id="login-username" name="username" required>
                </td>
            </tr>
            <tr>
                <td>
                    <fmt:message key="login.password"/>
                </td>
                <td>
                    <input type="password" id="login-password" name="password" required/>
                </td>
            </tr>
            <tr>
                <td>
                    <a href='<%=response.encodeURL("/resources/views/registration.jsp")%>'>
                        <fmt:message key="login.register"/>
                    </a>
                </td>
                <td>
                    <input name="submit" type="submit" value=<fmt:message key="login.submit"/> />
                    <input name="reset" type="reset" value=<fmt:message key="login.reset"/> />
                <td>
            </tr>
        </table>
    </form>
</body>
</html>


package com.epam.rd2017.dao.impl;

import com.epam.rd2017.dao.OrderDao;
import com.epam.rd2017.model.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OrderDaoImpl implements OrderDao {

    private static final Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);

    private static final String SELECT_ALL_SQL = "SELECT o.*, u.*, b.* FROM orders AS o " +
                                                 "JOIN users AS u ON o.user_id = u.user_id " +
                                                 "JOIN books AS b ON o.book_id = b.book_id ";

    private static final String BY_ID_SQL = " WHERE o.order_id = ?";

    private static final String BY_STATE_SQL = " WHERE o.state = ?";

    private static final String DELETE_SQL = "DELETE FROM orders WHERE order_id = ?";

    private static final String UPDATE_SQL = "UPDATE orders SET state = ? WHERE order_id = ?";

    private static final String INSERT_SQL = "INSERT INTO orders (user_id, book_id) VALUES (?, ?)";

    @Override
    public Order getById(int orderId) {
        Order order = null;

        try (Connection connection = DataBaseUtil.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL_SQL + BY_ID_SQL);
            statement.setInt(1, orderId);

            try (ResultSet result = statement.executeQuery()) {
                if (result.first()) {
                    order = DataBaseUtil.fillOrder(result, "o.");
                    order.setUser(DataBaseUtil.fillUser(result, "u."));
                    order.setBook(DataBaseUtil.fillBook(result, "b.", false));
                }
            } catch (SQLException ex) {
                logger.error("Error in retrieving order from database", ex);
                throw new RuntimeException(ex);
            }
        } catch (Exception ex) {
            logger.error("Can't get connection to database", ex);
            throw new RuntimeException(ex);
        }

        return order;
    }

    @Override
    public List<Order> getByState(String state) {
        List<Order> orders = new ArrayList<>();

        try (Connection connection = DataBaseUtil.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL_SQL + BY_STATE_SQL);
            statement.setString(1, state);

            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    Order order = DataBaseUtil.fillOrder(result, "o.");
                    order.setUser(DataBaseUtil.fillUser(result, "u."));
                    order.setBook(DataBaseUtil.fillBook(result, "b.", false));
                    orders.add(order);
                }
            } catch (SQLException ex) {
                logger.error("Error in retrieving orders from database", ex);
                throw new RuntimeException(ex);
            }
        } catch (Exception ex) {
            logger.error("Can't get connection to database", ex);
            throw new RuntimeException(ex);
        }

        return orders;
    }

    @Override
    public List<Order> getAll() {
        List<Order> orders = new ArrayList<>();

        try (Connection connection = DataBaseUtil.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL_SQL);

            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    Order order = DataBaseUtil.fillOrder(result, "o.");
                    order.setUser(DataBaseUtil.fillUser(result, "u."));
                    order.setBook(DataBaseUtil.fillBook(result, "b.", false));
                    orders.add(order);
                }
            } catch (SQLException ex) {
                logger.error("Error in retrieving orders from database", ex);
                throw new RuntimeException(ex);
            }
        } catch (Exception ex) {
            logger.error("Can't get connection to database", ex);
            throw new RuntimeException(ex);
        }

        return orders;
    }

    @Override
    public void save(Order order) {
        try (Connection connection = DataBaseUtil.getConnection()) {
            PreparedStatement statement = order.getId() > 0 ? update(order, connection) : create(order, connection);
            statement.execute();
        } catch (Exception ex) {
            logger.error("Can't get connection to database", ex);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void delete(Order order) {
        try (Connection connection = DataBaseUtil.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(DELETE_SQL);
            statement.setInt(1, order.getId());

            statement.execute();
        } catch (Exception ex) {
            logger.error("Can't get connection to database", ex);
            throw new RuntimeException(ex);
        }
    }

    private PreparedStatement update(Order order, Connection connection) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(UPDATE_SQL);
        statement.setString(1, order.getState());
        statement.setInt(2, order.getId());

        return statement;
    }

    private PreparedStatement create(Order order, Connection connection) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(INSERT_SQL);
        statement.setInt(1, order.getUser().getId());
        statement.setInt(2, order.getBook().getId());

        return statement;
    }
}

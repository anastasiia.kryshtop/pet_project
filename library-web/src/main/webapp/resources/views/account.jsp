<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/customtags/button.tld" prefix="b" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><fmt:message key="account.title"/></title>
</head>
<body>
    <h3 align="center">
        <fmt:message key="account.welcomeMessage"/>
    </h3>

    <table id="account-data">
        <tr>
            <td>
                <fmt:message key="account.name"/>
            </td>
            <td>
                <c:out value="${user.name}"/>
            </td>
        </tr>
        <tr>
            <td>
                <fmt:message key="account.creationDate"/>
            </td>
            <td>
                <c:out value="${user.creationDate}"/>
            </td>
        </tr>
        <tr>
            <td>
                <fmt:message key="account.role"/>
            </td>
            <td>
                <c:out value="${user.role}"/>
            </td>
        </tr>
        <tr>
            <td>
                 <fmt:message key="account.state"/>
            </td>
            <td>
                <c:if test="${user.blocked}">
                    <fmt:message key="account.state.blocked"/>
                </c:if>
                <c:if test="${!user.blocked}">
                    <fmt:message key="account.state.active"/>
                </c:if>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <c:if test="${user.role == 'admin'}">
                <td>
                    <b:button reference="/users" messageKey="account.users"/>
                </td>
            </c:if>
            <c:if test="${user.role == 'librarian'}">
                <td>
                    <b:button reference="/orders" messageKey="account.orders"/>
                </td>
                <td>
                    <b:button reference="/issues" messageKey="account.issuedBooks"/>
                </td>
            </c:if>
            <c:if test="${user.role == 'user'}">
                <td>
                    <b:button reference="/issues?forUser=true" messageKey="account.issuedBooks"/>
                </td>
            </c:if>
            <td>
                <b:button reference="/catalogue" messageKey="account.catalogue"/>
            </td>
            <td>
                <b:button reference="/logout" messageKey="account.logout"/>
            <td>
        </tr>
    </table>
</body>
</html>


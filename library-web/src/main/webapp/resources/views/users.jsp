<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/customtags/button.tld" prefix="b" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><fmt:message key="users.title"/></title>
</head>
<body>
    <h3 align="center">
        <fmt:message key="users.welcomeMessage"/>
    </h3>

    <c:set var="roles" value="${['admin', 'user', 'librarian']}"/>
    <form id="search-id" method="POST" action=<%=response.encodeURL("/searchusers")%>>
        <table>
            <tr>
                <td>
                    <input type="search" id="search-user" name="q" value="" placeholder='<fmt:message key="users.searchPlaceholder"/>'>
                </td>
                <td>
                    <input name="search" type="submit" value=<fmt:message key="users.search"/> />
                </td>
            </tr>
        </table>
    </form>
    <table>
        <tr>
            <td>
                <table>
                    <tr>
                        <th>
                            <fmt:message key="users.name"/>
                        </th>
                        <th>
                            <fmt:message key="users.role"/>
                        </th>
                        <th>
                            <fmt:message key="users.creationDate"/>
                        </th>
                        <th>
                            <fmt:message key="users.blocked"/>
                        </th>
                        <th>
                        </th>
                    </tr>
                    <c:forEach items="${users}" var="user">
                    <form id="users-id" method="POST" action=<%=response.encodeURL("/edituser")%>>
                        <tr>
                            <td>
                                <c:out value="${user.name}"/>
                            </td>
                            <td>
                                <select name="userRole">
                                    <c:forEach var="role" items="${roles}">
                                        <option value="${role}" ${role == user.role ? 'selected="selected"' : ''}>${role}</option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td>
                                <c:out value="${user.creationDate}"/>
                            </td>
                            <td>
                                <input type="checkbox" name="blocked" value="blocked" <c:if test="${user.blocked}">checked</c:if>/>
                            </td>
                            <td>
                                <button type="submit"><fmt:message key="users.save" /></button>
                                <b:button reference="/edituser?delete=true&userId=${user.id}" messageKey="users.delete"/>
                            </td>
                        </tr>
                        <input type="hidden" name="userId" value="${user.id}">
                    </form>
                    </c:forEach>
                </table>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <b:button reference="/resources/views/account.jsp" messageKey="users.back"/>
            </td>
            <td>
                <b:button reference="/logout" messageKey="users.logout"/>
            </td>
        </tr>
    </table>
</body>
</html>
package com.epam.rd2017.web.servlet;

import com.epam.rd2017.model.Issue;
import com.epam.rd2017.model.Order;
import com.epam.rd2017.model.OrderState;
import com.epam.rd2017.service.IssueService;
import com.epam.rd2017.service.OrderService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ProcessOrderServlet extends HttpServlet {

    private static final long serialVersionUID = 7787780598013236861L;
    private OrderService orderService;
    private IssueService issueService;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServletContext context = getServletContext();
        orderService = (OrderService) context.getAttribute("orderService");
        issueService = (IssueService) context.getAttribute("issueService");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String orderId = request.getParameter("orderId");
        String canceled = request.getParameter("canceled");
        RequestDispatcher requestDispatcher;

        if (orderId != null && !orderId.equals("")) {
            Order order = orderService.getById(Integer.valueOf(orderId));

            if (canceled == null || canceled.equals("") || !Boolean.valueOf(canceled)) {
                String issueState = request.getParameter("iState");
                Issue issue = new Issue();
                issue.setUser(order.getUser());
                issue.setBook(order.getBook());
                issue.setState(issueState);
                issueService.save(issue);

                order.setState(OrderState.PROCESSED);
            } else {
                order.setState(OrderState.CANCELED);
            }
            orderService.save(order);
        }

        requestDispatcher = request.getRequestDispatcher(response.encodeURL("/orders"));
        requestDispatcher.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}

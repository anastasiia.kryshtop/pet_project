<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ attribute name="error"  required="true" rtexprvalue="true" type="java.lang.String" %>
<%@ attribute name="messageKey"  required="true"  rtexprvalue="true" type="java.lang.String" %>

<c:if test="${not empty error}">
    <br/>
    <div>
        <fmt:message key="${messageKey}"/>: ${error}
    </div>
</c:if>
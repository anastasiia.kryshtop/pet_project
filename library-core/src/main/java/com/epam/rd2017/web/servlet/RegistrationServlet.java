package com.epam.rd2017.web.servlet;

import com.epam.rd2017.model.Role;
import com.epam.rd2017.model.User;
import com.epam.rd2017.service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;

public class RegistrationServlet extends HttpServlet {

    private static final long serialVersionUID = 3576398173760124010L;

    private UserService userService;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServletContext context = getServletContext();
        userService = (UserService)context.getAttribute("userService");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("username");
        String password = request.getParameter("password");
        String confirmPassword = request.getParameter("confirm");
        ResourceBundle messages = ResourceBundle.getBundle("messages");
        for (Enumeration<Locale> locales = request.getLocales(); locales.hasMoreElements();) {
            Locale locale = locales.nextElement();
            messages = ResourceBundle.getBundle("messages", locale);
            if (locale.equals(messages.getLocale()))
                break;
        }

        RequestDispatcher requestDispatcher = request.getRequestDispatcher(response.encodeURL("/resources/views/registration.jsp"));
        User dbUser = userService.getByName(name);
        if (dbUser != null) {
            request.setAttribute("error", messages.getString("registration.error.userAlreadyExists"));
        } else if (!password.equals(confirmPassword)) {
            request.setAttribute("error", messages.getString("registration.error.passwordsMismatch"));
        } else {
            User user = new User();
            user.setName(name);
            // TODO: encript password
            user.setPassword(password);
            user.setRole(Role.USER);
            userService.save(user);
            requestDispatcher = request.getRequestDispatcher(response.encodeURL("/resources/views/login.jsp"));
        }

        requestDispatcher.forward(request, response);
    }
}

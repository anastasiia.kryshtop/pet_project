package com.epam.rd2017.dao;

public interface DaoFactory {
    BookDao getBookDao();

    UserDao getUserDao();

    OrderDao getOrderDao();

    IssueDao getIssueDao();
}

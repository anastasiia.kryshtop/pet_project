package com.epam.rd2017.web.filter;

import com.epam.rd2017.model.Role;
import com.epam.rd2017.model.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.stream.Stream;

public class SecurityFilter implements Filter {

    // For jetty runner
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);

        String requestPath = request.getRequestURI();

        if (session == null || session.getAttribute("user") == null) {
            if (needsAuthentication(requestPath) ) {
                response.sendRedirect(request.getContextPath() + "/resources/views/login.jsp");
            } else {
                chain.doFilter(req, res);
            }
        } else {
            if (session.getAttribute("user") != null) {
                User user = (User)session.getAttribute("user");
                RequestDispatcher requestDispatcher = request.getRequestDispatcher(response.encodeURL("/resources/views/account.jsp"));
                if (isWelcomePages(requestPath) ||
                        (user.getRole().equals(Role.ADMIN) && (isLibrarianPages(requestPath) || isUserPages(requestPath))) ||
                        (user.getRole().equals(Role.LIBRARIAN) && isAdminPages(requestPath)) ||
                        (user.getRole().equals(Role.USER) && (isAdminPages(requestPath) || isLibrarianPages(requestPath)))) {
                    requestDispatcher.forward(request, response);
                } else {
                    chain.doFilter(req, res);
                }
            }
        }
    }

    private boolean needsAuthentication(String url) {
        return !isWelcomePages(url) && !isCataloguePage(url);
    }

    private boolean isAdminPages(String url) {
        return Stream.of("editbook", "users").anyMatch(w -> url.contains(w));
    }

    private boolean isLibrarianPages(String url) {
        return Stream.of("orders").anyMatch(w -> url.contains(w));
    }

    private boolean isUserPages(String url) {
        return Stream.of("issues").anyMatch(w -> url.contains(w));
    }

    private boolean isWelcomePages(String url) {
        return url.equals("/") || Stream.of("login", "registration", "index").anyMatch(w -> url.contains(w));
    }

    private boolean isCataloguePage(String url) {
        return url.contains("catalogue");
    }
}

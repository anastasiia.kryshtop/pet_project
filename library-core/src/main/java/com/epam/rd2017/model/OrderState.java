package com.epam.rd2017.model;

public interface OrderState {

    String NEW = "new";

    String PROCESSED = "processed";

    String CANCELED = "canceled";
}

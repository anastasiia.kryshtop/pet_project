package com.epam.rd2017.dao.impl;

import com.epam.rd2017.model.Book;
import com.epam.rd2017.model.Issue;
import com.epam.rd2017.model.Order;
import com.epam.rd2017.model.User;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class DataBaseUtil {
    /**
     * Gets a connection from the properties specified in the file
     * db.properties
     *
     * @return the database connection
     */
    public static Connection getConnection() throws IOException, SQLException, ClassNotFoundException {
        Properties props = new Properties();
        try (InputStream in =
                     BookDaoImpl.class.getClassLoader().getResourceAsStream("db.properties")) {
            props.load(in);
        }
        String drivers = props.getProperty("driver");
        if (drivers != null) {
            System.setProperty("driver", drivers);
        }
        String url = props.getProperty("url");
        String username = props.getProperty("user");
        String password = props.getProperty("password");

        Class.forName(drivers);
        return DriverManager.getConnection(url, username, password);
    }

    public static Book fillBook(ResultSet result, String alias, boolean forCatalogue) throws SQLException {
        Book book = new Book();
        book.setId(result.getInt(alias + "book_id"));
        book.setTitle(result.getString(alias + "title"));
        book.setAuthor(result.getString(alias + "author"));
        book.setEdition(result.getInt(alias + "edition"));
        book.setPublicationDate(result.getDate(alias + "publication_date"));
        book.setCopiesCount(result.getInt(alias + "copies_count"));
        book.setUsageTerm(result.getInt(alias + "usage_term"));
        book.setFinePerDay(result.getInt(alias + "fine_per_day"));
        if (forCatalogue) {
            book.setAvailableCopies(result.getInt(alias + "available_copies"));
            book.setOrdered(false);
        }
        return book;
    }

    public static User fillUser(ResultSet result, String alias) throws SQLException {
        User user = new User();
        user.setId(result.getInt(alias + "user_id"));
        user.setName(result.getString(alias + "name"));
        user.setPassword(result.getString(alias + "password"));
        user.setRole(result.getString(alias + "role"));
        user.setCreationDate(result.getDate(alias + "creation_date"));
        user.setBlocked(result.getBoolean(alias + "blocked"));

        return user;
    }

    public static Order fillOrder(ResultSet result, String alias) throws SQLException {
        Order order = new Order();
        order.setId(result.getInt(alias + "order_id"));
        order.setCreationDate(result.getDate(alias + "creation_date"));
        order.setState(result.getString(alias + "state"));

        return order;
    }

    public static Issue fillIssue(ResultSet result, String alias) throws SQLException {
        Issue issue = new Issue();
        issue.setId(result.getInt(alias + "issue_id"));
        issue.setCreationDate(result.getDate(alias + "creation_date"));
        issue.setReturnDate(result.getDate(alias + "return_date"));
        issue.setState(result.getString(alias + "state"));

        return issue;
    }
}

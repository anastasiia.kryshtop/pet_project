package com.epam.rd2017.model;

public class BookSortCrireria {

    private boolean title;

    private boolean author;

    private boolean edition;

    private boolean publicationDate;

    public BookSortCrireria() {
        title = false;
        author = false;
        edition = false;
        publicationDate = false;
    }

    public boolean isTitle() {
        return title;
    }

    public void setTitle(boolean title) {
        this.title = title;
    }

    public boolean isAuthor() {
        return author;
    }

    public void setAuthor(boolean author) {
        this.author = author;
    }

    public boolean isEdition() {
        return edition;
    }

    public void setEdition(boolean edition) {
        this.edition = edition;
    }

    public boolean isPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(boolean publicationDate) {
        this.publicationDate = publicationDate;
    }
}

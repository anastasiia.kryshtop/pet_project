package com.epam.rd2017.model;

public interface Role {

    String ADMIN = "admin";

    String USER = "user";

    String LIBRARIAN = "librarian";
}

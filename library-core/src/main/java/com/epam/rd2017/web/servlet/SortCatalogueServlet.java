package com.epam.rd2017.web.servlet;

import com.epam.rd2017.model.Book;
import com.epam.rd2017.model.BookSortCrireria;
import com.epam.rd2017.service.BookService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Stream;

public class SortCatalogueServlet extends HttpServlet {

    private static final long serialVersionUID = -5352031945007426170L;

    private BookService bookService;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServletContext context = getServletContext();
        bookService = (BookService)context.getAttribute("bookService");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BookSortCrireria bookSortCrireria = new BookSortCrireria();
        String[] checkedValues = request.getParameterValues("sortCriteria");
        if (isChecked("title", checkedValues)) {
            bookSortCrireria.setTitle(true);
        }
        if (isChecked("author", checkedValues)) {
            bookSortCrireria.setAuthor(true);
        }
        if (isChecked("edition", checkedValues)) {
            bookSortCrireria.setEdition(true);
        }
        if (isChecked("publicationDate", checkedValues)) {
            bookSortCrireria.setPublicationDate(true);
        }

        List<Book> books = bookService.getSorted(bookSortCrireria);
        request.setAttribute("books", books);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher(response.encodeURL("/resources/views/catalogue.jsp"));
        requestDispatcher.forward(request, response);
    }

    private boolean isChecked(String value, String[] checkedValues) {
        return Stream.of(checkedValues).anyMatch(w -> w.equals(value));
    }
}

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/customtags/button.tld" prefix="b" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><fmt:message key="issues.title"/></title>
</head>
<body>
    <h3 align="center">
        <fmt:message key="issues.welcomeMessage"/>
    </h3>

    <c:if test="${user.role == 'librarian'}">
        <form id="search-id" method="POST" action=<%=response.encodeURL("/searchissues")%>>
            <table>
                <tr>
                    <td>
                        <input type="search" id="search-user-issues" name="q" value="" placeholder='<fmt:message key="issues.searchPlaceholder"/>'>
                    </td>
                    <td>
                        <input name="search" type="submit" value=<fmt:message key="issues.search"/> />
                    </td>
                </tr>
            </table>
        </form>
    </c:if>
    <form id="issues-id" >
        <table>
            <tr>
                <td>
                    <table>
                        <tr>
                            <th>
                                <fmt:message key="issues.bookTitle"/>
                            </th>
                            <th>
                                <fmt:message key="issues.userName"/>
                            </th>
                            <th>
                                <fmt:message key="issues.creationDate"/>
                            </th>
                            <th>
                                <fmt:message key="issues.state"/>
                            </th>
                            <th>
                                <fmt:message key="issues.returnDate"/>
                            </th>
                            <th>
                                <fmt:message key="issues.expirationDate"/>
                            </th>
                            <th>
                                <fmt:message key="issues.fine"/>
                            </th>
                            <c:if test="${user.role == 'librarian'}">
                                <th>
                                </th>
                            </c:if>
                        </tr>
                        <c:forEach items="${issues}" var="issue">
                            <tr>
                                <td>
                                    <c:out value="${issue.book.title}"/>
                                </td>
                                <td>
                                    <c:out value="${issue.user.name}"/>
                                </td>
                                <td>
                                    <c:out value="${issue.creationDate}"/>
                                </td>
                                <td>
                                    <c:out value="${issue.state}"/>
                                </td>
                                <td>
                                    <c:out value="${issue.returnDate}"/>
                                </td>
                                <td>
                                    <c:out value="${issue.expirationDate}"/>
                                </td>
                                <td>
                                    <c:out value="${issue.fine}"/>
                                </td>
                                <c:if test="${(user.role == 'librarian') && (empty issue.returnDate)}">
                                    <td>
                                        <b:button reference="/returnissue?issueId=${issue.id}" messageKey="issues.return"/>
                                    </td>
                                </c:if>
                            </tr>
                        </c:forEach>
                    </table>
                </td>
            </tr>
        </table>
    </form>
    <table>
        <tr>
            <td>
                <b:button reference="/resources/views/account.jsp" messageKey="issues.back"/>
            </td>
            <td>
                <b:button reference="/logout" messageKey="issues.logout"/>
            </td>
        </tr>
    </table>
</body>
</html>
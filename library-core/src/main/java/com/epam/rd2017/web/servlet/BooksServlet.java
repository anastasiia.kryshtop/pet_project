package com.epam.rd2017.web.servlet;

import com.epam.rd2017.model.Book;
import com.epam.rd2017.service.BookService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class BooksServlet extends HttpServlet {

    private static final long serialVersionUID = -4617553581435032930L;

    private BookService bookService;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServletContext context = getServletContext();
        bookService = (BookService)context.getAttribute("bookService");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Book> books = bookService.getAll();
        request.getSession().setAttribute("books", books);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher(response.encodeURL("/resources/views/catalogue.jsp"));
        requestDispatcher.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}

package com.epam.rd2017.dao.impl;

import com.epam.rd2017.dao.BookDao;
import com.epam.rd2017.model.Book;
import com.epam.rd2017.model.BookSortCrireria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class BookDaoImpl implements BookDao {

    private static final Logger logger = LoggerFactory.getLogger(BookDaoImpl.class);

    private static final String SELECT_ALL_SQL = "select b.book_id AS book_id, " +
            "b.title AS title, " +
            "b.author AS author, " +
            "b.edition AS edition, " +
            "b.publication_date AS publication_date, " +
            "b.copies_count AS copies_count, " +
            "b.usage_term AS usage_term, " +
            "b.fine_per_day AS fine_per_day, " +
            "(b.copies_count - (SELECT COUNT(*) FROM issues WHERE b.book_id = book_id AND return_date IS NULL)) AS available_copies " +
            "FROM books AS b";

    private static final String ORDER_BY_SQL = " ORDER BY ";

    private static final String INSERT_SQL = "INSERT INTO books (title, author, edition, publication_date, copies_count, usage_term, fine_per_day) VALUES (?, ?, ?, ?, ?, ?, ?)";

    private static final String UPDATE_SQL = "UPDATE books SET title = ?, author = ?, edition = ?, publication_date = ?, copies_count = ?, usage_term = ?, fine_per_day = ? WHERE book_id = ?";

    private static final String SELECT_BY_ID_SQL = "SELECT * FROM books WHERE book_id = ?";

    @Override
    public Book getById(int bookId) {
        Book book = null;

        try (Connection connection = DataBaseUtil.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_BY_ID_SQL);
            statement.setInt(1, bookId);

            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    book = DataBaseUtil.fillBook(result, "", false);
                }
            } catch (SQLException ex) {
                logger.error("Error in retrieving books from database", ex);
                throw new RuntimeException(ex);
            }
        } catch (Exception ex) {
            logger.error("Can't get connection to database", ex);
            throw new RuntimeException(ex);
        }

        return book;
    }

    @Override
    public List<Book> getSorted(BookSortCrireria bookSortCrireria) {
        ArrayList<Book> books = new ArrayList<>();
        StringBuilder orderString = new StringBuilder();
        boolean isFirst = true;

        if (bookSortCrireria.isTitle()) {
            orderString.append(isFirst ? ORDER_BY_SQL : ", "). append("title");
            isFirst = false;
        }
        if (bookSortCrireria.isAuthor()) {
            orderString.append(isFirst ? ORDER_BY_SQL : ", "). append("author");
            isFirst = false;
        }
        if (bookSortCrireria.isEdition()) {
            orderString.append(isFirst ? ORDER_BY_SQL : ", "). append("edition");
            isFirst = false;
        }
        if (bookSortCrireria.isPublicationDate()) {
            orderString.append(isFirst ? ORDER_BY_SQL : ", "). append("publication_date");
            isFirst = false;
        }

        try (Connection connection = DataBaseUtil.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL_SQL + (isFirst ? "" : orderString.toString()));

            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    books.add(DataBaseUtil.fillBook(result, "", true));
                }
            } catch (SQLException ex) {
                logger.error("Error in retrieving books from database", ex);
                throw new RuntimeException(ex);
            }
        } catch (Exception ex) {
            logger.error("Can't get connection to database", ex);
            throw new RuntimeException(ex);
        }

        return books;
    }

    @Override
    public List<Book> searchByTitleOrAuthor(String searchString) {
        ArrayList<Book> books = new ArrayList<>();
        String searchSQL = " WHERE title LIKE '%" + searchString + "%' OR author LIKE '%" + searchString + "%'";

        try (Connection connection = DataBaseUtil.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL_SQL + searchSQL);

            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    books.add(DataBaseUtil.fillBook(result, "", true));
                }
            } catch (SQLException ex) {
                logger.error("Error in retrieving books from database", ex);
                throw new RuntimeException(ex);
            }
        } catch (Exception ex) {
            logger.error("Can't get connection to database", ex);
            throw new RuntimeException(ex);
        }

        return books;
    }

    @Override
    public List<Book> getAll() {
        List<Book> books = new ArrayList<>();

        try (Connection connection = DataBaseUtil.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL_SQL);

            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    books.add(DataBaseUtil.fillBook(result, "", true));
                }
            } catch (SQLException ex) {
                logger.error("Error in retrieving books from database", ex);
                throw new RuntimeException(ex);
            }
        } catch (Exception ex) {
            logger.error("Can't get connection to database", ex);
            throw new RuntimeException(ex);
        }

        logger.trace(String.format("Receive %d book(s)", books.size()));

        return books;
    }

    @Override
    public void save(Book book) {
        try (Connection connection = DataBaseUtil.getConnection()) {
            PreparedStatement statement = book.getId() > 0 ? update(book, connection) : create(book, connection);
            statement.execute();
        } catch (Exception ex) {
            logger.error("Can't get connection to database", ex);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void delete(Book book) {
        try (Connection connection = DataBaseUtil.getConnection()) {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM books WHERE book_id = ?");
            statement.setInt(1, book.getId());

            statement.execute();
        } catch (Exception ex) {
            logger.error("Can't get connection to database", ex);
            throw new RuntimeException(ex);
        }
    }

    private PreparedStatement update(Book book, Connection connection) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(UPDATE_SQL);
        statement.setString(1, book.getTitle());
        statement.setString(2, book.getAuthor());
        statement.setInt(3, book.getEdition());
        statement.setDate(4,  new java.sql.Date(book.getPublicationDate().getTime()));
        statement.setInt(5, book.getCopiesCount());
        statement.setInt(6, book.getUsageTerm());
        statement.setInt(7, book.getFinePerDay());
        statement.setInt(8, book.getId());

        return statement;
    }

    private PreparedStatement create(Book book, Connection connection) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(INSERT_SQL);
        statement.setString(1, book.getTitle());
        statement.setString(2, book.getAuthor());
        statement.setInt(3, book.getEdition());
        statement.setDate(4, new java.sql.Date(book.getPublicationDate().getTime()));
        statement.setInt(5, book.getCopiesCount());
        statement.setInt(6, book.getUsageTerm());
        statement.setInt(7, book.getFinePerDay());

        return statement;
    }
}

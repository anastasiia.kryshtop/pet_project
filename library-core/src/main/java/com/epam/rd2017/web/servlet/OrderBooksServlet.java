package com.epam.rd2017.web.servlet;

import com.epam.rd2017.model.Book;
import com.epam.rd2017.model.Order;
import com.epam.rd2017.model.User;
import com.epam.rd2017.service.BookService;
import com.epam.rd2017.service.OrderService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Stream;

public class OrderBooksServlet extends HttpServlet {

    private static final long serialVersionUID = 1156981441522357850L;

    private OrderService orderService;
    private BookService bookService;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServletContext context = getServletContext();
        orderService = (OrderService)context.getAttribute("orderService");
        bookService = (BookService)context.getAttribute("bookService");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = (User)request.getSession().getAttribute("user");
        String[] booksId = request.getParameterValues("booksToOrder");

        if (booksId != null && booksId.length > 0) {
            StringBuilder orderedBooks = new StringBuilder();
            Stream.of(booksId).forEach(id -> {
                Book book = bookService.getById(Integer.valueOf(id));
                Order order = new Order();
                order.setUser(user);
                order.setBook(book);
                if (orderedBooks.length() > 0) orderedBooks.append(", ");
                orderedBooks.append(book.getTitle());

                orderService.save(order);
            });

            request.setAttribute("orderedBooks", orderedBooks.toString());
        }

        RequestDispatcher requestDispatcher = request.getRequestDispatcher(response.encodeURL("/resources/views/catalogue.jsp"));
        requestDispatcher.forward(request, response);
    }
}

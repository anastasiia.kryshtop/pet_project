CREATE DATABASE IF NOT EXISTS library;

DROP TABLE IF EXISTS library.users;

CREATE TABLE library.users (
  user_id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  password varchar(255) NOT NULL,
  role varchar(225) NOT NULL,
  creation_date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  blocked tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (user_id),
  UNIQUE KEY name_UNIQUE (name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS library.books;

CREATE TABLE library.books (
  book_id int(11) NOT NULL AUTO_INCREMENT,
  title varchar(255) NOT NULL,
  author varchar(255) NOT NULL,
  edition int(11) NOT NULL,
  publication_date datetime NOT NULL,
  copies_count int(11) NOT NULL,
  usage_term int(11) NOT NULL,
  PRIMARY KEY (book_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS library.issues;

CREATE TABLE library.issues (
  issue_id int(11) NOT NULL AUTO_INCREMENT,
  user_id int(11) NOT NULL,
  book_id int(11) NOT NULL,
  creation_date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  return_date datetime DEFAULT NULL,
  mode varchar(45) NOT NULL,
  PRIMARY KEY (issue_id),
  KEY fk_issues_to_users_idx (user_id),
  KEY fk_issues_to_books_idx (book_id),
  CONSTRAINT fk_issues_to_books FOREIGN KEY (book_id) REFERENCES books (book_id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT fk_issues_to_users FOREIGN KEY (user_id) REFERENCES users (user_id) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO library.users (name, password, role) VALUES ('test', 'test', 'admin');


package com.epam.rd2017.web.servlet;

import com.epam.rd2017.model.User;
import com.epam.rd2017.service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class SearchUsersServlet extends HttpServlet {

    private static final long serialVersionUID = 976136207911159714L;
    private UserService userService;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServletContext context = getServletContext();
        userService = (UserService) context.getAttribute("userService");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String searchString = request.getParameter("q");

        List<User> users = userService.searchByName(searchString);
        request.setAttribute("users", users);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher(response.encodeURL("/resources/views/users.jsp"));
        requestDispatcher.forward(request, response);
    }
}

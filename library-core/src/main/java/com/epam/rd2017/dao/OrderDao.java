package com.epam.rd2017.dao;

import com.epam.rd2017.model.Order;

import java.util.List;

public interface OrderDao {
    Order getById(int orderId);

    List<Order> getByState(String state);

    List<Order> getAll();

    void save(Order order);

    void delete(Order order);
}

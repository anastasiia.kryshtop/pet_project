package com.epam.rd2017.dao;

import com.epam.rd2017.model.User;

import java.util.List;

public interface UserDao {
    User getById(int userId);

    User getByName(String name);

    List<User> searchByName(String searchString);

    List<User> getAll();

    void save(User user);

    void delete(User user);
}

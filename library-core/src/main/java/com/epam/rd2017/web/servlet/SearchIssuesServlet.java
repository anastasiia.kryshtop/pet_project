package com.epam.rd2017.web.servlet;

import com.epam.rd2017.model.Issue;
import com.epam.rd2017.service.IssueService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class SearchIssuesServlet extends HttpServlet {

    private static final long serialVersionUID = -5630872570257535702L;
    private IssueService issueService;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServletContext context = getServletContext();
        issueService = (IssueService) context.getAttribute("issueService");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String searchString = request.getParameter("q");

        List<Issue> issues = issueService.searchByUserName(searchString);
        request.setAttribute("issues", issues);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher(response.encodeURL("/resources/views/issues.jsp"));
        requestDispatcher.forward(request, response);
    }
}

package com.epam.rd2017.service;

import com.epam.rd2017.dao.DaoFactory;
import com.epam.rd2017.model.Issue;
import com.epam.rd2017.model.User;

import java.util.List;

public interface IssueService {
    void setIssueDao(DaoFactory daoFactory);

    Issue getById(int issueId);

    List<Issue> getByUser(User user);

    List<Issue> searchByUserName(String userName);

    List<Issue> getAllNotReturned();

    void save(Issue issue);

    void delete(Issue issue);
}

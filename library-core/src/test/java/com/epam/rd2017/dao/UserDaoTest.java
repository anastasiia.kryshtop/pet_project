package com.epam.rd2017.dao;

import com.epam.rd2017.dao.impl.DataBaseUtil;
import com.epam.rd2017.dao.impl.UserDaoImpl;
import com.epam.rd2017.model.Role;
import com.epam.rd2017.model.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

public class UserDaoTest {

    private UserDao userDao = new UserDaoImpl();

    private static final SimpleDateFormat formatter = new SimpleDateFormat(
            "dd/MM/yy");

    private static final String TEST_USERNAME = "test_user";
    private static final String TEST_USERNAME_1 = "test_user1";
    private static final String TEST_PASSWORD = "test_password";
    private static final String TEST_PASSWORD_1 = "test_password1";
    private static final String TEST_ROLE = Role.ADMIN;
    private static final String TEST_ROLE_1 = Role.LIBRARIAN;
    private static final boolean TEST_BLOCKED_1 = true;
    private static final Date TEST_DATE = new Date();

    @Before
    public void beforeTest() {
        releaseTable();
    }

    @After
    public void afterTest() {
        releaseTable();
    }

    @Test
    public void testSaveDelete() {
        User user = prepareTestUser();
        userDao.save(user);

        User dbUser = userDao.getByName(TEST_USERNAME);
        assertNotNull(dbUser);
        assertTrue(dbUser.getId() > 0);
        assertEquals(TEST_USERNAME, dbUser.getName());
        assertEquals(TEST_PASSWORD, dbUser.getPassword());
        assertEquals(TEST_ROLE, dbUser.getRole());
        assertNotNull(dbUser.getCreationDate());
        assertFalse(dbUser.isBlocked());

        userDao.delete(dbUser);
        dbUser = userDao.getByName(TEST_USERNAME);
        assertNull(dbUser);
    }

    @Test
    public void testUpdateUser() {
        User user = prepareTestUser();
        userDao.save(user);

        User dbUser = userDao.getByName(TEST_USERNAME);
        assertNotNull(dbUser);

        dbUser.setName(TEST_USERNAME_1);
        dbUser.setPassword(TEST_PASSWORD_1);
        dbUser.setRole(TEST_ROLE_1);
        dbUser.setCreationDate(TEST_DATE);
        dbUser.setBlocked(TEST_BLOCKED_1);
        userDao.save(dbUser);

        User updatedDbUser = userDao.getByName(TEST_USERNAME);
        assertNull(updatedDbUser);
        updatedDbUser = userDao.getByName(TEST_USERNAME_1);
        assertNotNull(updatedDbUser);
        assertEquals(dbUser.getId(), updatedDbUser.getId());
        assertEquals(TEST_USERNAME_1, updatedDbUser.getName());
        assertEquals(TEST_PASSWORD_1, updatedDbUser.getPassword());
        assertEquals(TEST_ROLE_1, updatedDbUser.getRole());
        assertEquals(formatter.format(TEST_DATE), formatter.format(updatedDbUser.getCreationDate()));
        assertEquals(TEST_BLOCKED_1, updatedDbUser.isBlocked());
    }

    @Test(expected = RuntimeException.class)
    public void testSaveWithTheSameName() {
        User user = prepareTestUser();
        userDao.save(user);

        User dbUser = userDao.getByName(TEST_USERNAME);
        assertNotNull(dbUser);

        userDao.save(user);
    }

    private User prepareTestUser() {
        User user = new User();
        user.setName(TEST_USERNAME);
        user.setPassword(TEST_PASSWORD);
        user.setRole(TEST_ROLE);

        return user;
    }

    private void releaseTable() {
        try (Connection connection = DataBaseUtil.getConnection()) {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM users WHERE name LIKE '" + TEST_USERNAME + "%'");
            statement.execute();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}

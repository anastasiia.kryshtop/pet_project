package com.epam.rd2017.web.servlet;

import com.epam.rd2017.model.Issue;
import com.epam.rd2017.model.User;
import com.epam.rd2017.service.IssueService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class IssuesServlet extends HttpServlet {

    private static final long serialVersionUID = -9011848421966653141L;

    private IssueService issueService;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServletContext context = getServletContext();
        issueService = (IssueService)context.getAttribute("issueService");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String forUser = request.getParameter("forUser");
        List<Issue> issues = null;

        if (forUser != null && !forUser.equals("") && Boolean.valueOf(forUser)) {
            issues = issueService.getByUser((User) request.getSession().getAttribute("user"));
        } else {
            issues = issueService.getAllNotReturned();
        }
        request.setAttribute("issues", issues);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher(response.encodeURL("/resources/views/issues.jsp"));
        requestDispatcher.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}

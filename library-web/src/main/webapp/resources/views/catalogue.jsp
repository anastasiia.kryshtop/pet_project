<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/customtags/button.tld" prefix="b" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><fmt:message key="catalogue.title"/></title>
</head>
<body>
    <h3 align="center">
        <fmt:message key="catalogue.welcomeMessage"/>
    </h3>

    <c:if test="${not empty orderedBooks}">
        <br/>
        <div>
            <fmt:message key="catalogue.order.successMessage"/>: ${orderedBooks}
        </div>
    </c:if>

    <form id="search-id" method="POST" action=<%=response.encodeURL("/searchcatalogue")%>>
        <table>
            <tr>
                <td>
                    <input type="search" id="search-catalogue" name="q" value="" placeholder='<fmt:message key="catalogue.searchPlaceholder"/>'>
                </td>
                <td>
                    <input name="search" type="submit" value=<fmt:message key="catalogue.search"/> />
                </td>
            </tr>
        </table>
    </form>
    <form id="sort-id" method="POST" action=<%=response.encodeURL("/sortcatalogue")%>>
        <table>
            <tr>
                <td>
                    <input name="sort" type="submit" value=<fmt:message key="catalogue.sort"/> />
                </td>
                <td>
                    <fmt:message key="catalogue.sort.title"/>
                </td>
                <td>
                    <input type="checkbox" name="sortCriteria" value="title"/>
                </td>
                <td>
                    <fmt:message key="catalogue.sort.author"/>
                </td>
                <td>
                    <input type="checkbox" name="sortCriteria" value="author"/>
                </td>
                <td>
                    <fmt:message key="catalogue.sort.edition"/>
                </td>
                <td>
                    <input type="checkbox" name="sortCriteria" value="edition"/>
                </td>
                <td>
                    <fmt:message key="catalogue.sort.publicationDate"/>
                </td>
                <td>
                    <input type="checkbox" name="sortCriteria" value="publicationDate"/>
                </td>
            </tr>
        </table>
    </form>
    <form id="catalogue-id" method="post" action=<%=response.encodeURL("/order")%>>
        <table>
            <tr>
                <td>
                    <table>
                        <tr>
                            <th>
                                <fmt:message key="catalogue.bookTitle"/>
                            </th>
                            <th>
                                <fmt:message key="catalogue.author"/>
                            </th>
                            <th>
                                <fmt:message key="catalogue.edition"/>
                            </th>
                            <th>
                                <fmt:message key="catalogue.publicationDate"/>
                            </th>
                            <th>
                                <fmt:message key="catalogue.usageTerm"/>
                            </th>
                            <th>
                                <fmt:message key="catalogue.finePerDay"/>
                            </th>
                            <th>
                                <fmt:message key="catalogue.availableCopies"/>
                            </th>
                            <c:if test="${not empty user}">
                                <c:if test="${user.role == 'user'}">
                                    <th>
                                        <fmt:message key="catalogue.order"/>
                                    </th>
                                </c:if>
                                <c:if test="${user.role == 'admin'}">
                                    <th>
                                    </th>
                                </c:if>
                            </c:if>
                        </tr>
                        <c:forEach items="${books}" var="book">
                            <tr>
                                <td>
                                    <c:out value="${book.title}"/>
                                </td>
                                <td>
                                    <c:out value="${book.author}"/>
                                </td>
                                <td>
                                    <c:out value="${book.edition}"/>
                                </td>
                                <td>
                                    <c:out value="${book.publicationDate}"/>
                                </td>
                                <td>
                                    <c:out value="${book.usageTerm}"/>
                                </td>
                                <td>
                                    <c:out value="${book.finePerDay}"/>
                                </td>
                                <td>
                                    <c:out value="${book.availableCopies}"/>
                                </td>
                                <c:if test="${(not empty user)}">
                                    <c:if test="${user.role == 'user'}">
                                        <td>
                                            <c:if test="${book.availableCopies > 0}">
                                                <input type="checkbox" name="booksToOrder" value="${book.id}"/>
                                            </c:if>
                                        </td>
                                    </c:if>
                                    <c:if test="${user.role == 'admin'}">
                                        <td>
                                            <b:button reference="/editbook?delete=false&bookId=${book.id}" messageKey="catalogue.editBook"/>
                                            <b:button reference="/editbook?delete=true&bookId=${book.id}" messageKey="catalogue.deleteBook"/>
                                        </td>
                                    </c:if>
                                </c:if>
                            </tr>
                        </c:forEach>
                    </table>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>
                    <c:if test="${not empty user}">
                        <c:if test="${user.role == 'admin'}">
                            <b:button reference="/editbook" messageKey="catalogue.addBook"/>
                        </c:if>
                        <c:if test="${user.role == 'user'}">
                            <button type="submit"><fmt:message key="catalogue.order"/></button>
                        </c:if>
                        <b:button reference="/resources/views/account.jsp" messageKey="catalogue.back"/>
                        <b:button reference="/logout" messageKey="catalogue.logout"/>
                    </c:if>
                    <c:if test="${empty user}">
                        <b:button reference="/resources/views/index.jsp" messageKey="catalogue.back"/>
                    </c:if>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
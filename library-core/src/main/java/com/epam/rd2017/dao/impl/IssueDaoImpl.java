package com.epam.rd2017.dao.impl;

import com.epam.rd2017.dao.IssueDao;
import com.epam.rd2017.model.Issue;
import com.epam.rd2017.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class IssueDaoImpl implements IssueDao {

    private static final Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);
    private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");

    private static final String SELECT_ALL_SQL = "SELECT i.*, u.*, b.* FROM issues AS i " +
                                                 "JOIN users AS u ON i.user_id = u.user_id " +
                                                 "JOIN books AS b ON i.book_id = b.book_id ";

    private static final String BY_ID_SQL = " WHERE i.issue_id = ?";

    private static final String BY_USER_ID_SQL = " WHERE i.user_id = ?";

    private static final String NOT_RETURNED_SQL = " WHERE i.return_date is NULL";

    private static final String ORDER_BY_RETURN_DATE = " ORDER BY i.return_date";

    private static final String DELETE_SQL = "DELETE FROM issues WHERE issue_id = ?";

    private static final String UPDATE_SQL = "UPDATE issues SET state = ?, return_date = ? WHERE issue_id = ?";

    private static final String INSERT_SQL = "INSERT INTO issues (user_id, book_id, state) VALUES (?, ?, ?)";

    @Override
    public Issue getById(int issueId) {
        Issue issue = null;

        try (Connection connection = DataBaseUtil.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL_SQL + BY_ID_SQL);
            statement.setInt(1, issueId);

            try (ResultSet result = statement.executeQuery()) {
                if (result.first()) {
                    issue = DataBaseUtil.fillIssue(result, "i.");
                    issue.setUser(DataBaseUtil.fillUser(result, "u."));
                    issue.setBook(DataBaseUtil.fillBook(result, "b.", false));
                    calcNotPersistentFields(issue);
                }
            } catch (SQLException ex) {
                logger.error("Error in retrieving issue from database", ex);
                throw new RuntimeException(ex);
            }
        } catch (Exception ex) {
            logger.error("Can't get connection to database", ex);
            throw new RuntimeException(ex);
        }

        return issue;
    }

    @Override
    public List<Issue> getByUser(User user) {
        List<Issue> issues = new ArrayList<>();

        try (Connection connection = DataBaseUtil.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL_SQL + BY_USER_ID_SQL + ORDER_BY_RETURN_DATE);
            statement.setInt(1, user.getId());

            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    Issue issue = DataBaseUtil.fillIssue(result, "i.");
                    issue.setUser(DataBaseUtil.fillUser(result, "u."));
                    issue.setBook(DataBaseUtil.fillBook(result, "b.", false));
                    calcNotPersistentFields(issue);
                    issues.add(issue);
                }
            } catch (SQLException ex) {
                logger.error("Error in retrieving issues from database", ex);
                throw new RuntimeException(ex);
            }
        } catch (Exception ex) {
            logger.error("Can't get connection to database", ex);
            throw new RuntimeException(ex);
        }

        return issues;
    }

    @Override
    public List<Issue> searchByUserName(String userName) {
        List<Issue> issues = new ArrayList<>();

        try (Connection connection = DataBaseUtil.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL_SQL + NOT_RETURNED_SQL + " AND u.name LIKE '%" + userName + "%' ORDER BY u.name");

            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    Issue issue = DataBaseUtil.fillIssue(result, "i.");
                    issue.setUser(DataBaseUtil.fillUser(result, "u."));
                    issue.setBook(DataBaseUtil.fillBook(result, "b.", false));
                    calcNotPersistentFields(issue);
                    issues.add(issue);
                }
            } catch (SQLException ex) {
                logger.error("Error in retrieving issues from database", ex);
                throw new RuntimeException(ex);
            }
        } catch (Exception ex) {
            logger.error("Can't get connection to database", ex);
            throw new RuntimeException(ex);
        }

        return issues;
    }

    @Override
    public List<Issue> getAllNotReturned() {
        List<Issue> issues = new ArrayList<>();

        try (Connection connection = DataBaseUtil.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL_SQL + NOT_RETURNED_SQL);

            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    Issue issue = DataBaseUtil.fillIssue(result, "i.");
                    issue.setUser(DataBaseUtil.fillUser(result, "u."));
                    issue.setBook(DataBaseUtil.fillBook(result, "b.", false));
                    calcNotPersistentFields(issue);
                    issues.add(issue);
                }
            } catch (SQLException ex) {
                logger.error("Error in retrieving issues from database", ex);
                throw new RuntimeException(ex);
            }
        } catch (Exception ex) {
            logger.error("Can't get connection to database", ex);
            throw new RuntimeException(ex);
        }

        return issues;
    }

    @Override
    public void save(Issue issue) {
        try (Connection connection = DataBaseUtil.getConnection()) {
            PreparedStatement statement = issue.getId() > 0 ? update(issue, connection) : create(issue, connection);
            statement.execute();
        } catch (Exception ex) {
            logger.error("Can't get connection to database", ex);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void delete(Issue issue) {
        try (Connection connection = DataBaseUtil.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(DELETE_SQL);
            statement.setInt(1, issue.getId());

            statement.execute();
        } catch (Exception ex) {
            logger.error("Can't get connection to database", ex);
            throw new RuntimeException(ex);
        }
    }

    private PreparedStatement update(Issue issue, Connection connection) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(UPDATE_SQL);
        statement.setString(1, issue.getState());
        statement.setDate(2, new java.sql.Date(issue.getReturnDate().getTime()));
        statement.setInt(3, issue.getId());

        return statement;
    }

    private PreparedStatement create(Issue issue, Connection connection) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(INSERT_SQL);
        statement.setInt(1, issue.getUser().getId());
        statement.setInt(2, issue.getBook().getId());
        statement.setString(3, issue.getState());

        return statement;
    }

    private void calcNotPersistentFields(Issue issue) {
        issue.setExpirationDate(new java.sql.Date(issue.getCreationDate().getTime() + TimeUnit.DAYS.toMillis(issue.getBook().getUsageTerm())));

        if (issue.getReturnDate() == null || issue.getReturnDate().compareTo(issue.getExpirationDate()) > 0) {
            long difference = (new Date()).getTime() - issue.getExpirationDate().getTime();
            long days = difference / (24 * 60 * 60 * 1000);
            issue.setFine(days > 0 ? (int) days * issue.getBook().getFinePerDay() : 0);
        } else {
            issue.setFine(0);
        }

        return;
    }
}

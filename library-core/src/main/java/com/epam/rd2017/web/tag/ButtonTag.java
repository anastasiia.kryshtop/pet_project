package com.epam.rd2017.web.tag;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;

public class ButtonTag extends TagSupport {

    private String reference;
    private String messageKey;

    @Override
    public int doStartTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpServletResponse response = (HttpServletResponse) pageContext.getResponse();

        ResourceBundle messages = ResourceBundle.getBundle("messages");
        for (Enumeration<Locale> locales = pageContext.getRequest().getLocales(); locales.hasMoreElements();) {
            Locale locale = locales.nextElement();
            messages = ResourceBundle.getBundle("messages", locale);
            if (locale.equals(messages.getLocale()))
                break;
        }

        try {
            out.print("<button type=\"button\" onclick=\"location.href='");
            out.print(response.encodeURL(reference));
            out.print("'\">");
            out.print(messages.getString(messageKey));
            out.print("</button>");
        } catch (IOException ex) {
            throw new JspException(ex);
        }

        return SKIP_BODY;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getMessageKey() {
        return messageKey;
    }

    public void setMessageKey(String messageKey) {
        this.messageKey = messageKey;
    }
}
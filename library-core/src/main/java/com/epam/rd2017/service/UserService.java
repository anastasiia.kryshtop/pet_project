package com.epam.rd2017.service;

import com.epam.rd2017.dao.DaoFactory;
import com.epam.rd2017.model.User;

import java.util.List;

public interface UserService {
    void setUserDao(DaoFactory daoFactory);

    User getById(int userId);

    User getByName(String name);

    List<User> searchByName(String searchString);

    List<User> getAll();

    void save(User user);

    void delete(User user);
}

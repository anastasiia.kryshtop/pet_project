package com.epam.rd2017.service.impl;

import com.epam.rd2017.dao.DaoFactory;
import com.epam.rd2017.dao.UserDao;
import com.epam.rd2017.model.User;
import com.epam.rd2017.service.UserService;

import java.util.List;

public class UserServiceImpl implements UserService {

    private UserDao userDao;

    @Override
    public void setUserDao(DaoFactory daoFactory) {
        this.userDao = daoFactory.getUserDao();
    }

    @Override
    public User getById(int userId) {
        return userDao.getById(userId);
    }

    @Override
    public User getByName(String name) {
        return userDao.getByName(name);
    }

    @Override
    public List<User> searchByName(String searchString) {
        return userDao.searchByName(searchString);
    }

    @Override
    public List<User> getAll() {
        return userDao.getAll();
    }

    @Override
    public void save(User user) {
        userDao.save(user);
    }

    @Override
    public void delete(User user) {
        userDao.delete(user);
    }
}

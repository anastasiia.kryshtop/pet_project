package com.epam.rd2017.service;

import com.epam.rd2017.dao.DaoFactory;

public interface ServiceFactory {
    BookService getBookService(DaoFactory daoFactory);

    UserService getUserService(DaoFactory daoFactory);

    OrderService getOrderService(DaoFactory daoFactory);

    IssueService getIssueService(DaoFactory daoFactory);
}

package com.epam.rd2017.web.servlet;

import com.epam.rd2017.model.User;
import com.epam.rd2017.service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;

public class LoginServlet extends HttpServlet {

    private static final long serialVersionUID = 559437115301017715L;

    private UserService userService;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServletContext context = getServletContext();
        userService = (UserService)context.getAttribute("userService");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("username");
        String password = request.getParameter("password");
        ResourceBundle messages = ResourceBundle.getBundle("messages");
        for (Enumeration<Locale> locales = request.getLocales(); locales.hasMoreElements();) {
            Locale locale = locales.nextElement();
            messages = ResourceBundle.getBundle("messages", locale);
            if (locale.equals(messages.getLocale()))
                break;
        }

        User user = userService.getByName(name);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher(response.encodeURL("/resources/views/login.jsp"));

        if (user == null)
            request.setAttribute("error", messages.getString("login.error.userNotFound"));
        else if (!user.getPassword().equals(password))
            request.setAttribute("error", messages.getString("login.error.invalidPassword"));
        else {
            HttpSession session = request.getSession();
            session.setAttribute("user", user);
            requestDispatcher = request.getRequestDispatcher(response.encodeURL("/resources/views/account.jsp"));
        }

        requestDispatcher.forward(request, response);
    }
}

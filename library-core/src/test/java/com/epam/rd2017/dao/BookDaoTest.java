package com.epam.rd2017.dao;

import com.epam.rd2017.dao.impl.BookDaoImpl;
import com.epam.rd2017.dao.impl.DataBaseUtil;
import com.epam.rd2017.model.Book;
import com.epam.rd2017.model.BookSortCrireria;
import com.epam.rd2017.model.Role;
import com.epam.rd2017.model.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class BookDaoTest {

    private BookDao bookDao = new BookDaoImpl();

    private static final SimpleDateFormat formatter = new SimpleDateFormat(
            "dd/MM/yy");

    private static final String TEST_TITLE = "test_title";
    private static final String TEST_TITLE_1 = "test_title1";
    private static final String TEST_AUTHOR = "test_author";
    private static final String TEST_AUTHOR_1 = "test_author1";
    private static final int TEST_EDITION = 5;
    private static final int TEST_EDITION_1 = 3;
    private static final Date TEST_PUBLICATION_DATE = new Date();
    private static final Date TEST_PUBLICATION_DATE_1 = new Date(TEST_PUBLICATION_DATE.getTime() + TimeUnit.DAYS.toMillis(1));
    private static final int TEST_COPIES_COUNT = 1;
    private static final int TEST_COPIES_COUNT_1 = 10;
    private static final int TEST_USAGE_TERM = 20;
    private static final int TEST_USAGE_TERM_1 = 30;

    @Before
    public void beforeTest() {
        releaseTable();
    }

    @After
    public void afterTest() {
        releaseTable();
    }

    @Test
    public void testSaveDelete() {
        Book book = prepareTestBook();
        bookDao.save(book);

        List<Book> dbBooks = bookDao.searchByTitleOrAuthor(TEST_TITLE);
        assertNotNull(dbBooks);
        assertEquals(1, dbBooks.size());

        Book dbBook = dbBooks.get(0);
        assertTrue(dbBook.getId() > 0);
        assertEquals(TEST_TITLE, dbBook.getTitle());
        assertEquals(TEST_AUTHOR, dbBook.getAuthor());
        assertEquals(TEST_EDITION, dbBook.getEdition());
        assertEquals(formatter.format(TEST_PUBLICATION_DATE), formatter.format(dbBook.getPublicationDate()));
        assertEquals(TEST_COPIES_COUNT, dbBook.getCopiesCount());
        assertEquals(TEST_USAGE_TERM, dbBook.getUsageTerm());

        bookDao.delete(dbBook);
        dbBooks = bookDao.searchByTitleOrAuthor(TEST_TITLE);
        assertNotNull(dbBooks);
        assertEquals(0, dbBooks.size());
    }

    @Test
    public void testUpdate() {
        Book book = prepareTestBook();
        bookDao.save(book);

        List<Book> dbBooks = bookDao.searchByTitleOrAuthor(TEST_TITLE);
        assertNotNull(dbBooks);
        assertEquals(1, dbBooks.size());

        Book dbBook = dbBooks.get(0);
        assertTrue(dbBook.getId() > 0);
        dbBook.setTitle(TEST_TITLE_1);
        dbBook.setAuthor(TEST_AUTHOR_1);
        dbBook.setEdition(TEST_EDITION_1);
        dbBook.setPublicationDate(TEST_PUBLICATION_DATE_1);
        dbBook.setCopiesCount(TEST_COPIES_COUNT_1);
        dbBook.setUsageTerm(TEST_USAGE_TERM_1);
        bookDao.save(dbBook);

        Book updatedDbBook = bookDao.getById(dbBook.getId());
        assertNotNull(updatedDbBook);
        assertEquals(dbBook.getId(), updatedDbBook.getId());
        assertEquals(TEST_TITLE_1, updatedDbBook.getTitle());
        assertEquals(TEST_AUTHOR_1, updatedDbBook.getAuthor());
        assertEquals(TEST_EDITION_1, updatedDbBook.getEdition());
        assertEquals(formatter.format(TEST_PUBLICATION_DATE_1), formatter.format(updatedDbBook.getPublicationDate()));
        assertEquals(TEST_COPIES_COUNT_1, updatedDbBook.getCopiesCount());
        assertEquals(TEST_USAGE_TERM_1, updatedDbBook.getUsageTerm());
    }

    @Test
    public void testGetById() {
        Book book = prepareTestBook();
        bookDao.save(book);

        List<Book> dbBooks = bookDao.searchByTitleOrAuthor(TEST_TITLE);
        assertNotNull(dbBooks);
        assertEquals(1, dbBooks.size());

        Book dbBook = dbBooks.get(0);
        assertTrue(dbBook.getId() > 0);

        Book foundDbBook = bookDao.getById(dbBook.getId());
        assertNotNull(foundDbBook);
        assertEquals(dbBook.getId(), foundDbBook.getId());
    }

    @Test
    public void testGetAll() {
        Book book = prepareTestBook();
        Book book1 = prepareTestBook();
        book1.setTitle(TEST_TITLE_1);

        bookDao.save(book);
        bookDao.save(book1);

        List<Book> dbBooks = bookDao.getAll();
        assertNotNull(dbBooks);
        assertTrue(dbBooks.size() >= 2);

        Book dbBook = findByTitle(TEST_TITLE, dbBooks);
        assertNotNull(dbBook);
        assertTrue(dbBook.getId() > 0);
        assertEquals(TEST_TITLE, dbBook.getTitle());

        Book dbBook1 = findByTitle(TEST_TITLE_1, dbBooks);
        assertNotNull(dbBook1);
        assertTrue(dbBook1.getId() > 0);
        assertNotEquals(dbBook.getId(), dbBook1.getId());
        assertEquals(TEST_TITLE_1, dbBook1.getTitle());
    }

    @Test
    public void testGetSortedTitleCriteria() {
        Book book = prepareTestBook();
        Book book1 = prepareTestBook();
        book1.setTitle(TEST_TITLE_1);
        Book book2 = prepareTestBook();

        bookDao.save(book);
        bookDao.save(book1);
        bookDao.save(book2);

        BookSortCrireria crireria = new BookSortCrireria();
        crireria.setTitle(true);

        List<Book> dbBooks = bookDao.getSorted(crireria);
        assertNotNull(dbBooks);
        assertTrue(dbBooks.size() >= 3);

        List<Book> testDbBooks = findTestBooks(dbBooks);
        assertNotNull(testDbBooks);
        assertEquals(3, testDbBooks.size());

        assertEquals(TEST_TITLE, testDbBooks.get(0).getTitle());
        assertEquals(TEST_TITLE, testDbBooks.get(1).getTitle());
        assertEquals(TEST_TITLE_1, testDbBooks.get(2).getTitle());
    }

    @Test
    public void testGetSortedAuthorCriteria() {
        Book book = prepareTestBook();
        Book book1 = prepareTestBook();
        book1.setAuthor(TEST_AUTHOR_1);
        Book book2 = prepareTestBook();

        bookDao.save(book);
        bookDao.save(book1);
        bookDao.save(book2);

        BookSortCrireria crireria = new BookSortCrireria();
        crireria.setAuthor(true);

        List<Book> dbBooks = bookDao.getSorted(crireria);
        assertNotNull(dbBooks);
        assertTrue(dbBooks.size() >= 3);

        List<Book> testDbBooks = findTestBooks(dbBooks);
        assertNotNull(testDbBooks);
        assertEquals(3, testDbBooks.size());

        assertEquals(TEST_AUTHOR, testDbBooks.get(0).getAuthor());
        assertEquals(TEST_AUTHOR, testDbBooks.get(1).getAuthor());
        assertEquals(TEST_AUTHOR_1, testDbBooks.get(2).getAuthor());
    }

    @Test
    public void testGetSortedEditionCriteria() {
        Book book = prepareTestBook();
        Book book1 = prepareTestBook();
        book1.setEdition(TEST_EDITION_1);
        Book book2 = prepareTestBook();

        bookDao.save(book);
        bookDao.save(book1);
        bookDao.save(book2);

        BookSortCrireria crireria = new BookSortCrireria();
        crireria.setEdition(true);

        List<Book> dbBooks = bookDao.getSorted(crireria);
        assertNotNull(dbBooks);
        assertTrue(dbBooks.size() >= 3);

        List<Book> testDbBooks = findTestBooks(dbBooks);
        assertNotNull(testDbBooks);
        assertEquals(3, testDbBooks.size());

        assertEquals(TEST_EDITION_1, testDbBooks.get(0).getEdition());
        assertEquals(TEST_EDITION, testDbBooks.get(1).getEdition());
        assertEquals(TEST_EDITION, testDbBooks.get(2).getEdition());
    }

    @Test
    public void testGetSortedPublicationDateCriteria() {
        Book book = prepareTestBook();
        Book book1 = prepareTestBook();
        book1.setPublicationDate(TEST_PUBLICATION_DATE_1);
        Book book2 = prepareTestBook();

        bookDao.save(book);
        bookDao.save(book1);
        bookDao.save(book2);

        BookSortCrireria crireria = new BookSortCrireria();
        crireria.setPublicationDate(true);

        List<Book> dbBooks = bookDao.getSorted(crireria);
        assertNotNull(dbBooks);
        assertTrue(dbBooks.size() >= 3);

        List<Book> testDbBooks = findTestBooks(dbBooks);
        assertNotNull(testDbBooks);
        assertEquals(3, testDbBooks.size());

        assertEquals(formatter.format(TEST_PUBLICATION_DATE), formatter.format(testDbBooks.get(0).getPublicationDate()));
        assertEquals(formatter.format(TEST_PUBLICATION_DATE), formatter.format(testDbBooks.get(1).getPublicationDate()));
        assertEquals(formatter.format(TEST_PUBLICATION_DATE_1), formatter.format(testDbBooks.get(2).getPublicationDate()));
    }

    @Test
    public void testGetSortedAllCriteria() {
        Book book = prepareTestBook();
        Book book1 = prepareTestBook();
        book1.setTitle(TEST_TITLE_1);
        Book book2 = prepareTestBook();
        book2.setAuthor(TEST_AUTHOR_1);
        Book book3 = prepareTestBook();
        book3.setTitle(TEST_TITLE_1);
        book3.setEdition(TEST_EDITION_1);
        Book book4 = prepareTestBook();
        book4.setAuthor(TEST_AUTHOR_1);
        book4.setPublicationDate(TEST_PUBLICATION_DATE_1);

        bookDao.save(book);
        bookDao.save(book1);
        bookDao.save(book2);
        bookDao.save(book3);
        bookDao.save(book4);

        // book : test_title  test_author  5 now()
        // book2: test_title  test_author1 5 now()
        // book4: test_title  test_author1 5 now() + 1 day
        // book3: test_title1 test_author  3 now()
        // book1: test_title1 test_author  5 now()

        BookSortCrireria crireria = new BookSortCrireria();
        crireria.setTitle(true);
        crireria.setAuthor(true);
        crireria.setEdition(true);
        crireria.setPublicationDate(true);

        List<Book> dbBooks = bookDao.getSorted(crireria);
        assertNotNull(dbBooks);
        assertTrue(dbBooks.size() >= 5);

        List<Book> testDbBooks = findTestBooks(dbBooks);
        assertNotNull(testDbBooks);
        assertEquals(5, testDbBooks.size());

        assertEquals(TEST_TITLE, testDbBooks.get(0).getTitle());
        assertEquals(TEST_AUTHOR, testDbBooks.get(0).getAuthor());
        assertEquals(TEST_EDITION, testDbBooks.get(0).getEdition());
        assertEquals(formatter.format(TEST_PUBLICATION_DATE), formatter.format(testDbBooks.get(0).getPublicationDate()));

        assertEquals(TEST_TITLE, testDbBooks.get(1).getTitle());
        assertEquals(TEST_AUTHOR_1, testDbBooks.get(1).getAuthor());
        assertEquals(TEST_EDITION, testDbBooks.get(1).getEdition());
        assertEquals(formatter.format(TEST_PUBLICATION_DATE), formatter.format(testDbBooks.get(1).getPublicationDate()));

        assertEquals(TEST_TITLE, testDbBooks.get(2).getTitle());
        assertEquals(TEST_AUTHOR_1, testDbBooks.get(2).getAuthor());
        assertEquals(TEST_EDITION, testDbBooks.get(2).getEdition());
        assertEquals(formatter.format(TEST_PUBLICATION_DATE_1), formatter.format(testDbBooks.get(2).getPublicationDate()));

        assertEquals(TEST_TITLE_1, testDbBooks.get(3).getTitle());
        assertEquals(TEST_AUTHOR, testDbBooks.get(3).getAuthor());
        assertEquals(TEST_EDITION_1, testDbBooks.get(3).getEdition());
        assertEquals(formatter.format(TEST_PUBLICATION_DATE), formatter.format(testDbBooks.get(3).getPublicationDate()));

        assertEquals(TEST_TITLE_1, testDbBooks.get(4).getTitle());
        assertEquals(TEST_AUTHOR, testDbBooks.get(4).getAuthor());
        assertEquals(TEST_EDITION, testDbBooks.get(4).getEdition());
        assertEquals(formatter.format(TEST_PUBLICATION_DATE), formatter.format(testDbBooks.get(4).getPublicationDate()));
    }

    @Test
    public void testSearchByTitleOrAuthorTitleMatches() {
        Book book = prepareTestBook();
        Book book1 = prepareTestBook();
        book1.setTitle(TEST_TITLE_1);

        bookDao.save(book);
        bookDao.save(book1);

        List<Book> dbBooks = bookDao.searchByTitleOrAuthor(TEST_TITLE);
        assertNotNull(dbBooks);
        assertEquals(2, dbBooks.size());

        Book dbBook = findByTitle(TEST_TITLE, dbBooks);
        assertNotNull(dbBook);
        assertEquals(TEST_TITLE, dbBook.getTitle());

        Book dbBook1 = findByTitle(TEST_TITLE_1, dbBooks);
        assertNotNull(dbBook1);
        assertEquals(TEST_TITLE_1, dbBook1.getTitle());

        dbBooks = bookDao.searchByTitleOrAuthor(TEST_TITLE_1);
        assertNotNull(dbBooks);
        assertEquals(1, dbBooks.size());

        dbBook = findByTitle(TEST_TITLE, dbBooks);
        assertNull(dbBook);

        dbBook = findByTitle(TEST_TITLE_1, dbBooks);
        assertNotNull(dbBook);
        assertEquals(TEST_TITLE_1, dbBook.getTitle());

        dbBooks = bookDao.searchByTitleOrAuthor(TEST_TITLE_1 + "2");
        assertNotNull(dbBook);
        assertEquals(0, dbBooks.size());
    }

    @Test
    public void testSearchByTitleOrAuthorAuthorMatches() {
        Book book = prepareTestBook();
        Book book1 = prepareTestBook();
        book1.setAuthor(TEST_AUTHOR_1);

        bookDao.save(book);
        bookDao.save(book1);

        List<Book> dbBooks = bookDao.searchByTitleOrAuthor(TEST_AUTHOR);
        assertNotNull(dbBooks);
        assertEquals(2, dbBooks.size());

        Book dbBook = findByAuthor(TEST_AUTHOR, dbBooks);
        assertNotNull(dbBook);
        assertEquals(TEST_AUTHOR, dbBook.getAuthor());

        Book dbBook1 = findByAuthor(TEST_AUTHOR_1, dbBooks);
        assertNotNull(dbBook1);
        assertEquals(TEST_AUTHOR_1, dbBook1.getAuthor());

        dbBooks = bookDao.searchByTitleOrAuthor(TEST_AUTHOR_1);
        assertNotNull(dbBooks);
        assertEquals(1, dbBooks.size());

        dbBook = findByAuthor(TEST_AUTHOR, dbBooks);
        assertNull(dbBook);

        dbBook = findByAuthor(TEST_AUTHOR_1, dbBooks);
        assertNotNull(dbBook);
        assertEquals(TEST_AUTHOR_1, dbBook.getAuthor());

        dbBooks = bookDao.searchByTitleOrAuthor(TEST_AUTHOR_1 + "2");
        assertNotNull(dbBook);
        assertEquals(0, dbBooks.size());
    }

    @Test
    public void testSearchByTitleOrAuthor() {
        Book book = prepareTestBook();
        Book book1 = prepareTestBook();
        book1.setTitle(TEST_AUTHOR);
        book1.setAuthor(TEST_TITLE_1);

        bookDao.save(book);
        bookDao.save(book1);

        List<Book> dbBooks = bookDao.searchByTitleOrAuthor(TEST_TITLE);
        assertNotNull(dbBooks);
        assertEquals(2, dbBooks.size());

        Book dbBook = findByTitle(TEST_TITLE, dbBooks);
        assertNotNull(dbBook);
        assertEquals(TEST_TITLE, dbBook.getTitle());

        Book dbBook1 = findByTitle(TEST_AUTHOR, dbBooks);
        assertNotNull(dbBook1);
        assertEquals(TEST_TITLE_1, dbBook1.getAuthor());

        dbBooks = bookDao.searchByTitleOrAuthor(TEST_TITLE_1);
        assertNotNull(dbBooks);
        assertEquals(1, dbBooks.size());

        dbBook = findByTitle(TEST_TITLE, dbBooks);
        assertNull(dbBook);

        dbBook = findByTitle(TEST_AUTHOR, dbBooks);
        assertNotNull(dbBook);
        assertEquals(TEST_TITLE_1, dbBook.getAuthor());

        dbBooks = bookDao.searchByTitleOrAuthor(TEST_TITLE_1 + "2");
        assertNotNull(dbBook);
        assertEquals(0, dbBooks.size());
    }

    private Book prepareTestBook() {
        Book book = new Book();
        book.setTitle(TEST_TITLE);
        book.setAuthor(TEST_AUTHOR);
        book.setEdition(TEST_EDITION);
        book.setPublicationDate(TEST_PUBLICATION_DATE);
        book.setCopiesCount(TEST_COPIES_COUNT);
        book.setUsageTerm(TEST_USAGE_TERM);

        return book;
    }

    private void releaseTable() {
        try (Connection connection = DataBaseUtil.getConnection()) {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM books WHERE title LIKE '" + TEST_TITLE + "%' OR author LIKE '" + TEST_TITLE + "%'");
            statement.execute();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    private Book findByTitle(String title, List<Book> books) {
        return books.stream().filter((b) -> title.equals(b.getTitle())).findFirst().orElse(null);
    }

    private Book findByAuthor(String author, List<Book> books) {
        return books.stream().filter((b) -> author.equals(b.getAuthor())).findFirst().orElse(null);
    }

    private List<Book> findTestBooks(List<Book> books) {
        return books.stream().filter((b) -> b.getTitle().startsWith(TEST_TITLE)).collect(Collectors.toList());
    }
}

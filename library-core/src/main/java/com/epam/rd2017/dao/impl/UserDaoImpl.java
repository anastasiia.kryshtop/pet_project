package com.epam.rd2017.dao.impl;

import com.epam.rd2017.dao.UserDao;
import com.epam.rd2017.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements UserDao {

    private static final Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);

    private static final String SELECT_BY_ID_SQL = "SELECT * FROM users WHERE user_id = ?";

    private static final String SELECT_BY_NAME_SQL = "SELECT * FROM users WHERE name = ?";

    private static final String SEARCH_BY_NAME_SQL = "SELECT * FROM users WHERE name LIKE ";

    private static final String SELECT_ALL_SQL = "SELECT * FROM users";

    private static final String DELETE_SQL = "DELETE FROM users WHERE user_id = ?";

    private static final String UPDATE_SQL = "UPDATE users SET name = ?, password = ?, role = ?, blocked = ? WHERE user_id = ?";

    private static final String INSERT_SQL = "INSERT INTO users (name, password, role) VALUES (?, ?, ?)";

    @Override
    public User getById(int userId) {
        User user = null;

        try (Connection connection = DataBaseUtil.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_BY_ID_SQL);
            statement.setInt(1, userId);

            try (ResultSet result = statement.executeQuery()) {
                if (result.first()) {
                    user = DataBaseUtil.fillUser(result, "");
                }
            } catch (SQLException ex) {
                logger.error("Error in retrieving user from database", ex);
                throw new RuntimeException(ex);
            }
        } catch (Exception ex) {
            logger.error("Can't get connection to database", ex);
            throw new RuntimeException(ex);
        }

        return user;
    }

    @Override
    public User getByName(String name) {
        User user = null;

        try (Connection connection = DataBaseUtil.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_BY_NAME_SQL);
            statement.setString(1, name);

            try (ResultSet result = statement.executeQuery()) {
                if (result.first()) {
                    user = DataBaseUtil.fillUser(result, "");
                }
            } catch (SQLException ex) {
                logger.error("Error in retrieving users from database", ex);
                throw new RuntimeException(ex);
            }
        } catch (Exception ex) {
            logger.error("Can't get connection to database", ex);
            throw new RuntimeException(ex);
        }

        return user;
    }

    @Override
    public List<User> searchByName(String searchString) {
        List<User> users = new ArrayList<>();

        try (Connection connection = DataBaseUtil.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SEARCH_BY_NAME_SQL + "'%" + searchString + "%'");

            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    users.add(DataBaseUtil.fillUser(result, ""));
                }
            } catch (SQLException ex) {
                logger.error("Error in retrieving user from database", ex);
                throw new RuntimeException(ex);
            }
        } catch (Exception ex) {
            logger.error("Can't get connection to database", ex);
            throw new RuntimeException(ex);
        }

        return users;
    }

    @Override
    public List<User> getAll() {
        List<User> users = new ArrayList<>();

        try (Connection connection = DataBaseUtil.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL_SQL);

            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    users.add(DataBaseUtil.fillUser(result, ""));
                }
            } catch (SQLException ex) {
                logger.error("Error in retrieving users from database", ex);
                throw new RuntimeException(ex);
            }
        } catch (Exception ex) {
            logger.error("Can't get connection to database", ex);
            throw new RuntimeException(ex);
        }

        return users;
    }


    @Override
    public void save(User user) {
        try (Connection connection = DataBaseUtil.getConnection()) {
            PreparedStatement statement = user.getId() > 0 ? update(user, connection) : create(user, connection);
            statement.execute();
        } catch (Exception ex) {
            logger.error("Can't get connection to database", ex);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void delete(User user) {
        try (Connection connection = DataBaseUtil.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(DELETE_SQL);
            statement.setInt(1, user.getId());

            statement.execute();
        } catch (Exception ex) {
            logger.error("Can't get connection to database", ex);
            throw new RuntimeException(ex);
        }
    }

    private PreparedStatement update(User user, Connection connection) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(UPDATE_SQL);
        statement.setString(1, user.getName());
        statement.setString(2, user.getPassword());
        statement.setString(3, user.getRole());
        statement.setBoolean(4, user.isBlocked());
        statement.setInt(5, user.getId());

        return statement;
    }

    private PreparedStatement create(User user, Connection connection) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(INSERT_SQL);
        statement.setString(1, user.getName());
        statement.setString(2, user.getPassword());
        statement.setString(3, user.getRole());

        return statement;
    }
}

package com.epam.rd2017.web.servlet;

import com.epam.rd2017.model.Order;
import com.epam.rd2017.model.OrderState;
import com.epam.rd2017.service.OrderService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class OrdersServlet extends HttpServlet {

    private static final long serialVersionUID = 6863457146480462836L;

    private OrderService orderService;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServletContext context = getServletContext();
        orderService = (OrderService)context.getAttribute("orderService");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Order> orders = orderService.getByState(OrderState.NEW);
        request.setAttribute("orders", orders);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher(response.encodeURL("/resources/views/orders.jsp"));
        requestDispatcher.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}

package com.epam.rd2017.dao.impl;

import com.epam.rd2017.dao.*;

public class DaoFactoryImpl implements DaoFactory {
    @Override
    public BookDao getBookDao() {
        return new BookDaoImpl();
    }

    @Override
    public UserDao getUserDao() {
        return new UserDaoImpl();
    }

    @Override
    public OrderDao getOrderDao() {
        return new OrderDaoImpl();
    }

    @Override
    public IssueDao getIssueDao() {
        return new IssueDaoImpl();
    }
}

package com.epam.rd2017.service.impl;

import com.epam.rd2017.dao.BookDao;
import com.epam.rd2017.dao.DaoFactory;
import com.epam.rd2017.model.Book;
import com.epam.rd2017.model.BookSortCrireria;
import com.epam.rd2017.service.BookService;

import java.util.List;

public class BookServiceImpl implements BookService {

    private BookDao bookDao;

    @Override
    public void setBookDao(DaoFactory daoFactory) {
        this.bookDao = daoFactory.getBookDao();
    }

    @Override
    public Book getById(int bookId) {
        return bookDao.getById(bookId);
    }

    @Override
    public List<Book> getSorted(BookSortCrireria bookSortCrireria) {
        return bookDao.getSorted(bookSortCrireria);
    }

    @Override
    public List<Book> searchByTitleOrAuthor(String searchString) {
        return bookDao.searchByTitleOrAuthor(searchString);
    }

    @Override
    public List<Book> getAll() {
        return bookDao.getAll();
    }

    @Override
    public void save(Book book) {
        bookDao.save(book);
    }

    @Override
    public void delete(Book book) {
        bookDao.delete(book);
    }
}

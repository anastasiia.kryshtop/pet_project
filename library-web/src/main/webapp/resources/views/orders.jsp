<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/customtags/button.tld" prefix="b" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><fmt:message key="orders.title"/></title>
</head>
<body>
    <h3 align="center">
        <fmt:message key="orders.welcomeMessage"/>
    </h3>

    <c:set var="issueStates" value="${['loan', 'reading hall']}"/>
    <table>
        <tr>
            <td>
                <table>
                    <tr>
                        <th>
                            <fmt:message key="orders.bookTitle"/>
                        </th>
                        <th>
                            <fmt:message key="orders.userName"/>
                        </th>
                        <th>
                            <fmt:message key="orders.creationDate"/>
                        </th>
                        <th>
                            <fmt:message key="orders.issueState"/>
                        </th>
                        <th>
                        </th>
                    </tr>
                    <c:forEach items="${orders}" var="order">
                    <form id="orders-id" method="post" action="/processorder">
                        <tr>
                            <td>
                                <c:out value="${order.book.title}"/>
                            </td>
                            <td>
                                <c:out value="${order.user.name}"/>
                            </td>
                            <td>
                                <c:out value="${order.creationDate}"/>
                            </td>
                            <td>
                                <select name="iState">
                                    <c:forEach var="issueState" items="${issueStates}">
                                        <option value="${issueState}">${issueState}</option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td>
                                <button type="submit"><fmt:message key="orders.issue"/></button>
                                <b:button reference="/processorder?canceled=true&orderId=${order.id}" messageKey="orders.cancel"/>
                            </td>
                        </tr>
                        <input type="hidden" name="orderId" value="${order.id}">
                    </form>
                    </c:forEach>
                </table>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <b:button reference="/resources/views/account.jsp" messageKey="orders.back"/>
            </td>
            <td>
                <b:button reference="/logout" messageKey="orders.logout"/>
            </td>
        </tr>
    </table>
</body>
</html>
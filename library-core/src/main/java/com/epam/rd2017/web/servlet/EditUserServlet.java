package com.epam.rd2017.web.servlet;

import com.epam.rd2017.model.User;
import com.epam.rd2017.service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class EditUserServlet extends HttpServlet {

    private static final long serialVersionUID = -1868598938928856296L;
    private UserService userService;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServletContext context = getServletContext();
        userService = (UserService) context.getAttribute("userService");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userId = request.getParameter("userId");
        String delete = request.getParameter("delete");
        RequestDispatcher requestDispatcher;

        if (userId != null && !userId.equals("")) {
            User user = userService.getById(Integer.valueOf(userId));

            if (delete != null && !delete.equals("") && Boolean.valueOf(delete)) {
                userService.delete(user);
            } else {
                user.setRole(request.getParameter("userRole"));
                user.setBlocked(request.getParameter("blocked") != null);
                userService.save(user);
            }
        }

        requestDispatcher = request.getRequestDispatcher(response.encodeURL("/users"));
        requestDispatcher.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}

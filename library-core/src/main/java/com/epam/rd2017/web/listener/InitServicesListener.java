package com.epam.rd2017.web.listener;

import com.epam.rd2017.dao.DaoFactory;
import com.epam.rd2017.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class InitServicesListener implements ServletContextListener {

    private static final Logger logger = LoggerFactory.getLogger(InitServicesListener.class);

    @Override
    public void contextInitialized(ServletContextEvent event) {
        ServletContext servletContext = event.getServletContext();
        try {
            DaoFactory daoFactory = (DaoFactory)Class.forName(servletContext.getInitParameter("dao-factory")).newInstance();
            ServiceFactory serviceFactory = (ServiceFactory)Class.forName(servletContext.getInitParameter("service-factory")).newInstance();

            UserService userService = serviceFactory.getUserService(daoFactory);
            BookService bookService = serviceFactory.getBookService(daoFactory);
            OrderService orderService = serviceFactory.getOrderService(daoFactory);
            IssueService issueService = serviceFactory.getIssueService(daoFactory);

            servletContext.setAttribute("userService", userService);
            servletContext.setAttribute("bookService", bookService);
            servletContext.setAttribute("orderService", orderService);
            servletContext.setAttribute("issueService", issueService);
        } catch (Exception ex) {
            logger.error("Failure during initialization of services", ex);
            throw new RuntimeException(ex);
        }
    }
}

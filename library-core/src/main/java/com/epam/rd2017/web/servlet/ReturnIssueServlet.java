package com.epam.rd2017.web.servlet;

import com.epam.rd2017.model.Issue;
import com.epam.rd2017.service.IssueService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

public class ReturnIssueServlet extends HttpServlet {

    private static final long serialVersionUID = -8992630785436698235L;
    private IssueService issueService;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServletContext context = getServletContext();
        issueService = (IssueService) context.getAttribute("issueService");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String issueId = request.getParameter("issueId");
        RequestDispatcher requestDispatcher;

        if (issueId != null && !issueId.equals("")) {
            Issue issue = issueService.getById(Integer.valueOf(issueId));
            issue.setReturnDate(new Date());
            issueService.save(issue);
        }

        requestDispatcher = request.getRequestDispatcher(response.encodeURL("/issues"));
        requestDispatcher.forward(request, response);
    }
}

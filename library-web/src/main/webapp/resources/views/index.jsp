<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/customtags/button.tld" prefix="b" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><fmt:message key="welcome.title"/></title>
</head>
<body>
    <table>
        <tr>
            <td>
                <b:button reference="/resources/views/login.jsp" messageKey="welcome.login"/>
            </td>
            <td>
                <b:button reference="/resources/views/registration.jsp" messageKey="welcome.registration"/>
            </td>
            <td>
                <b:button reference="/catalogue" messageKey="welcome.catalogue"/>
            </td>
        </tr>
    </table>
</body>
</html>


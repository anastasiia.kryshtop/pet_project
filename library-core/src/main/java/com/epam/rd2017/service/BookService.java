package com.epam.rd2017.service;

import com.epam.rd2017.dao.DaoFactory;
import com.epam.rd2017.model.Book;
import com.epam.rd2017.model.BookSortCrireria;

import java.util.List;

public interface BookService {
    void setBookDao(DaoFactory daoFactory);

    Book getById(int bookId);

    List<Book> getSorted(BookSortCrireria bookSortCrireria);

    List<Book> searchByTitleOrAuthor(String searchString);

    List<Book> getAll();

    void save(Book book);

    void delete(Book book);
}

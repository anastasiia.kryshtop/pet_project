package com.epam.rd2017.dao;

import com.epam.rd2017.model.Issue;
import com.epam.rd2017.model.User;

import java.util.List;

public interface IssueDao {
    Issue getById(int issueId);

    List<Issue> getByUser(User user);

    List<Issue> searchByUserName(String userName);

    List<Issue> getAllNotReturned();

    void save(Issue issue);

    void delete(Issue issue);
}

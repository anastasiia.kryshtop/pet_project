package com.epam.rd2017.service.impl;

import com.epam.rd2017.dao.DaoFactory;
import com.epam.rd2017.dao.OrderDao;
import com.epam.rd2017.model.Order;
import com.epam.rd2017.service.OrderService;

import java.util.List;

public class OrderServiceImpl implements OrderService {

    private OrderDao orderDao;

    @Override
    public void setOrderDao(DaoFactory daoFactory) {
        this.orderDao = daoFactory.getOrderDao();
    }

    @Override
    public Order getById(int orderId) {
        return orderDao.getById(orderId);
    }

    @Override
    public List<Order> getByState(String state) {
        return orderDao.getByState(state);
    }

    @Override
    public List<Order> getAll() {
        return orderDao.getAll();
    }

    @Override
    public void save(Order order) {
        orderDao.save(order);
    }

    @Override
    public void delete(Order order) {
        orderDao.delete(order);
    }
}

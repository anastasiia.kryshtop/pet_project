package com.epam.rd2017.service;

import com.epam.rd2017.dao.DaoFactory;
import com.epam.rd2017.model.Order;

import java.util.List;

public interface OrderService {
    void setOrderDao(DaoFactory daoFactory);

    Order getById(int orderId);

    List<Order> getByState(String state);

    List<Order> getAll();

    void save(Order order);

    void delete(Order order);
}

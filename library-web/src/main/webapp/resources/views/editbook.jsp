<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/customtags/button.tld" prefix="b" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><fmt:message key="editbook.title"/></title>
</head>
<body>
    <h3 align="center">
        <fmt:message key="editbook.welcomeMessage"/>
    </h3>

    <form id="edit-book-form" name="f" method="POST" action=<%=response.encodeURL("/editbook")%>>
        <table>
            <tr>
                <td>
                    <fmt:message key="editbook.bookTitle"/>
                </td>
                <td>
                    <input type="text" name="title" value="${book.title}" required minlength="3" maxlength="255">
                </td>
            </tr>
            <tr>
                <td>
                    <fmt:message key="editbook.author"/>
                </td>
                <td>
                    <input type="text" name="author" value="${book.author}" required minlength="3" maxlength="255">
                </td>
            </tr>
            <tr>
                <td>
                    <fmt:message key="editbook.edition"/>
                </td>
                <td>
                    <input type="number" name="edition" required value="${book.edition}" required min="1">
                </td>
            </tr>
            <tr>
                <td>
                    <fmt:message key="editbook.publicationDate"/>
                </td>
                <td>
                    <input type="date" name="publicationDate" value="${book.publicationDate}" required>
                </td>
            </tr>
            <tr>
                <td>
                    <fmt:message key="editbook.copiesCount"/>
                </td>
                <td>
                    <input type="number" name="copiesCount" value="${book.copiesCount}" required min="1">
                </td>
            </tr>
            <tr>
                <td>
                    <fmt:message key="editbook.usageTerm"/>
                </td>
                <td>
                    <input type="number" name="usageTerm" value="${book.usageTerm}" required min="1">
                </td>
            </tr>
            <tr>
                <td>
                    <fmt:message key="editbook.finePerDay"/>
                </td>
                <td>
                    <input type="number" name="finePerDay" value="${book.finePerDay}" required min="0">
                </td>
            </tr>
            <tr>
                <td>
                    <b:button reference="/catalogue" messageKey="editbook.back"/>
                </td>
                <td>
                    <input type="submit" name="save"  value=<fmt:message key="editbook.save"/> />
                </td>
            </tr>
        </table>
        <input type="hidden" name="bookId" value="${book.id}">
    </form>
</body>
</html>


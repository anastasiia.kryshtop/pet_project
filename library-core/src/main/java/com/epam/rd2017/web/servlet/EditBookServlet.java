package com.epam.rd2017.web.servlet;

import com.epam.rd2017.model.Book;
import com.epam.rd2017.service.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class EditBookServlet extends HttpServlet {

    private static final long serialVersionUID = -2630996912778707719L;
    private static final Logger logger = LoggerFactory.getLogger(EditBookServlet.class);
    private static final SimpleDateFormat formatter = new SimpleDateFormat(
            "yyyy-mm-dd");

    private BookService bookService;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServletContext context = getServletContext();
        bookService = (BookService)context.getAttribute("bookService");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String bookId = request.getParameter("bookId");
        String delete = request.getParameter("delete");
        Book book = new Book();
        RequestDispatcher requestDispatcher;

        if (bookId != null && !bookId.equals("")) {
            book = bookService.getById(Integer.valueOf(bookId));

            if (delete != null && !delete.equals("") && Boolean.valueOf(delete)) {
                bookService.delete(book);
                requestDispatcher = request.getRequestDispatcher(response.encodeURL("/catalogue"));
                requestDispatcher.forward(request, response);
            }
        }

        request.setAttribute("book", book);
        requestDispatcher = request.getRequestDispatcher(response.encodeURL("/resources/views/editbook.jsp"));
        requestDispatcher.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Book book = new Book();
        book.setId(Integer.valueOf(request.getParameter("bookId")));
        book.setTitle(request.getParameter("title"));
        book.setAuthor(request.getParameter("author"));
        book.setEdition(Integer.valueOf(request.getParameter("edition")));
        try {
            book.setPublicationDate(formatter.parse(request.getParameter("publicationDate")));
        } catch (ParseException ex) {
            logger.error("Publication date was not parsed", ex);
            throw new RuntimeException(ex);
        }
        book.setCopiesCount(Integer.valueOf(request.getParameter("copiesCount")));
        book.setUsageTerm(Integer.valueOf(request.getParameter("usageTerm")));
        book.setFinePerDay(Integer.valueOf(request.getParameter("finePerDay")));

        bookService.save(book);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher(response.encodeURL("/catalogue"));
        requestDispatcher.forward(request, response);
    }
}

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="e" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><fmt:message key="registration.title"/></title>
</head>
<body>
    <h3 align="center">
        <fmt:message key="registration.welcomeMessage"/>
    </h3>

    <e:error error="${error}" messageKey="registration.error.failMessage"/>

    <form id="registration-form" name="f" method="POST" action=<%=response.encodeURL("/registration")%>>
        <table id="registration-data">
            <tr>
                <td>
                    <fmt:message key="registration.user"/>
                </td>
                <td>
                    <input id="registration-username" type="text" name="username" required minlength="3" maxlength="255">
                </td>
            </tr>
            <tr>
                <td>
                    <fmt:message key="registration.password"/>
                </td>
                <td>
                    <input id="registration-password" type="password" name="password" required minlength="3" maxlength="255"/>
                </td>
            </tr>
            <tr>
                <td>
                    <fmt:message key="registration.confirmPassword"/>
                </td>
                <td>
                    <input id="confirm-password" type="password" name="confirm" required minlength="3" maxlength="255"/>
                </td>
            </tr>
            <tr>
                <td>
                <a href='<%=response.encodeURL("login.jsp")%>'>
                        <fmt:message key="registration.login"/>
                    </a>
                </td>
                <td>
                    <input name="save" type="submit" value=<fmt:message key="registration.save"/> />
                    <input name="reset" type="reset" value=<fmt:message key="registration.reset"/> />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>


package com.epam.rd2017.service.impl;

import com.epam.rd2017.dao.DaoFactory;
import com.epam.rd2017.service.*;

public class ServiceFactoryImpl implements ServiceFactory {
    @Override
    public BookService getBookService(DaoFactory daoFactory) {
        BookService bookService = new BookServiceImpl();
        bookService.setBookDao(daoFactory);

        return bookService;
    }

    @Override
    public UserService getUserService(DaoFactory daoFactory) {
        UserService userService = new UserServiceImpl();
        userService.setUserDao(daoFactory);

        return userService;
    }

    @Override
    public OrderService getOrderService(DaoFactory daoFactory) {
        OrderService orderService = new OrderServiceImpl();
        orderService.setOrderDao(daoFactory);

        return orderService;
    }

    @Override
    public IssueService getIssueService(DaoFactory daoFactory) {
        IssueService issueService = new IssueServiceImpl();
        issueService.setIssueDao(daoFactory);

        return issueService;
    }
}

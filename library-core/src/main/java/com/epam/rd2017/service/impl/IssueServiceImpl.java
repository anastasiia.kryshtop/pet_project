package com.epam.rd2017.service.impl;

import com.epam.rd2017.dao.DaoFactory;
import com.epam.rd2017.dao.IssueDao;
import com.epam.rd2017.model.Issue;
import com.epam.rd2017.model.User;
import com.epam.rd2017.service.IssueService;

import java.util.List;

public class IssueServiceImpl implements IssueService {

    private IssueDao issueDao;

    @Override
    public void setIssueDao(DaoFactory daoFactory) {
        this.issueDao = daoFactory.getIssueDao();
    }

    @Override
    public Issue getById(int issueId) {
        return issueDao.getById(issueId);
    }

    @Override
    public List<Issue> getByUser(User user) {
        return issueDao.getByUser(user);
    }

    @Override
    public List<Issue> searchByUserName(String userName) {
        return issueDao.searchByUserName(userName);
    }

    @Override
    public List<Issue> getAllNotReturned() {
        return issueDao.getAllNotReturned();
    }

    @Override
    public void save(Issue issue) {
        issueDao.save(issue);
    }

    @Override
    public void delete(Issue issue) {
        issueDao.delete(issue);
    }
}
